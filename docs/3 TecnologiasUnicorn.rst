.. _Cloud Ingenieria:

*************************************************
Software engineering, DevOps  and Cloud Computing 
*************************************************

*DevOps and the cloud are closely related. The vast majority of cloud development projects use DevOps, and the list will get longer and longer. The benefits of using DevOps with cloud projects are also being better defined. The following resources provide relevant information on the methods of software development process related to Devops and particularly for the case of Cloud Computing.*

* :ref:`GeneralArticulosDIS`
* :ref:`GeneralArticulosIS`
* :ref:`GeneralVideosIS`
* :ref:`GeneralCoursesIS`
* :ref:`GeneralBlogsIS`
