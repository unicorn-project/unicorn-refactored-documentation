.. _GeneralArticulosD:

Magazine articles
#################

.. topic:: Magazine articles, Cloud Computing:

   **Title**: A Cloud You Can Trust

   **Source**: https://spectrum.ieee.org/computing/hardware/the-cloud-is-the-computer
                                              
   **Author**: Christian Cachin and Matthias Schunterterm

   **Date**: November 2011

   **Summary**: This past April, Amazon’s Elastic Compute Cloud service crashed during a system upgrade, knocking customers’ websites off-line for anywhere from several hours to several days. That same month, hackers broke into the Sony PlayStation Network, exposing the personal information of 77 million people around the world. And in June a software glitch at cloud-storage provider Dropbox temporarily allowed visitors to log in to any of its 25 million customers’ accounts using any password—or none at all. As a company blogger drily noted: “This should never have happened.”

   **Target audience**:  Startups/SMEs

   **Difficulty**: Medium
#########################

.. topic:: Magazine articles, Cloud Computing:

   **Title**: Pros and Cons of Cloud Computing Technology 

   **Source**: https://www.ijsr.net/archive/v5i7/ART2016314.pdf
                                              
   **Author**: Sandeep Mukherji and Shashwat Srivastava

   **Date**: July de 2016

   **Summary**: Cloud computing technology today is the most enchanting technology that has become something for every business users or plans to move to. There are different types of cloud beneficial for the different users of the World, for the simple public cloud is most suitable, for the corporate world private cloud raise its hand and for the other users who are not completely corporate nor public hybrid cloud is chosen, all these three categories make all the uses complete for the world. Cloud computing is a boon for the business solution that every enterprise wants to adopt it. But having lots of pros in cloud computing, there are some cons also. This Paper is trying to put some light on those areas. 

   **Target audience**: Startups/SMEs

   **Difficulty**: Medium
#########################

.. topic:: Magazine articles, Cloud Computing:

   **Title**: The Cloud Is The Computer

   **Source**: https://spectrum.ieee.org/computing/hardware/the-cloud-is-the-computer
                                         
   **Author**: Paul McFedries

   **Date**: Aug 2018

   **Summary**: In the past few years we've seen Sun's slogan morph from perplexing to prophetic. As we do more and more online, we see that the network--that is, the Internet--is now an extension of our computers, to say the least. Particularly with wireless technologies, we see that a big chunk of our computing lives now sits out there in that haze of data and connections known as the cloud . In fact, we're on the verge of cloud computing , in which not just our data but even our software resides within the cloud, and we access everything not only through our PCs but also cloud-friendly devices, such as smart phones, PDAs, computing appliances, gaming consoles, even cars.

   **Target audience**:  Startups/SMEs

   **Difficulty**: Medium
#########################

.. topic:: Magazine articles, Cloud Computing:

   **Title**: Escape From the Data Center: The Promise of Peer-to-Peer Cloud Computing

   **Source**: https://spectrum.ieee.org/computing/networks/escape-from-the-data-center-the-promise-of-peertopeer-cloud-computing
                                         
   **Author**: Ozalp Babaoglu and Moreno Marzolla

   **Date**: Sep 2014

   **Summary**: Today, cloud computing takes place in giant server farms owned by the likes of Amazon, Google, or Microsoft—but it doesn’t have to.Not long ago, any start-up hoping to create the next big thing on the Internet had to invest sizable amounts of money in computing hardware, network connectivity, real estate to house the equipment, and technical personnel to keep everything working 24/7. The inevitable delays in getting all this funded, designed, and set up could easily erase any competitive edge the company might have had at the outset.

   **Target audience**:  Startups/SMEs

   **Difficulty**: Medium
#########################

.. topic:: Magazine articles, Cloud Computing:

   **Title**: A Comprehensive Guide to Secure Cloud Computing.ISBN:0470589876 9780470589878

   **Source**: https://www.amazon.es/Cloud-Security-Comprehensive-Secure-Computing/dp/0470589876
                                         
   **Author**: Ronald L. KrutzRussell Dean Vines

   **Date**: Sep 2014

   **Summary**: Well-known security experts decipher the most challenging aspect of cloud computing-security Cloud computing allows for both large and small organizations to have the opportunity to use Internet-based services so that they can reduce start-up costs, lower capital expenditures, use services on a pay-as you-use basis, access applications only as needed, and quickly reduce or increase capacities. However, these benefits are accompanied by a myriad of security issues, and this valuable book tackles the most common security challenges that cloud computing faces. The authors offer you years of unparalleled expertise and knowledge as they discuss the extremely challenging topics of data ownership, privacy protections, data mobility, quality of service and service levels, bandwidth costs, data protection, and support. As the most current and complete guide to helping you find your way through a maze of security minefields, this book is mandatory reading if you are involved in any aspect of cloud computing. Coverage Includes: Cloud Computing Fundamentals Cloud Computing Architecture Cloud Computing Software Security Fundamentals Cloud Computing Risks Issues Cloud Computing Security Challenges Cloud Computing Security Architecture Cloud Computing Life Cycle Issues Useful Next Steps and Approaches).

   **Target audience**:  Startups/SMEs

   **Difficulty**: Medium
#########################