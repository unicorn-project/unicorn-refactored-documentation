.. _GeneralCoursesAdopcion:

Courses
#######

.. topic:: Courses, Cloud Computing Adoption:

   **Title**: DOCKER FUNDAMENTALS + ENTERPRISE DEVELOPERS COURSE BUNDLE

   **Source**: https://training.docker.com/instructor-led-training/docker-fundamentals-enterprise-developers
                                              
   **Author**: Docker

   **Date**: 2017

   **Summary**: As the follow-on to the Docker Fundamentals course, Docker for Enterprise Operations is a role-based course designed for an organization’s Development and DevOps teams to accelerate their Docker journey in the enterprise. The course covers in-depth core advanced features of Docker EE and best practices to apply these features at scale with enterprise workloads. It is highly recommended to complete the Docker Fundamentals course as a pre-requisite.

   **Target audience**:  Startups/SMEs 

   **Difficulty**: Medium
#########################

.. topic:: Courses, Cloud Computing Adoption:

   **Title**: General docker courses

   **Source**: https://training.docker.com/instructor-led-training
                                              
   **Author**: Docker

   **Date**: 2017

   **Summary**: Docker Fundamentals: (2 days, $1495) - https://success.docker.com/training/courses/docker-fundamentals

This is THE introductory Docker course to give your team the best foundation for enterprise grade Docker use-cases.

Docker for Enterprise Developers: (2 days, $1995) - https://success.docker.com/training/courses/docker-for-enterprise-developers

As the follow-on to our Docker Fundamentals course, Docker for Enterprise Developers is a role-based course designed for an organization’s Development and DevOps teams to accelerate their Docker journey in the enterprise.

Docker for Enterprise Operations:(2 Days, $1995) - https://success.docker.com/trainingcourses/docker-for-enterprise-operations

This course is the second level of Docker's core curriculum for the enterprise and is focused on the Docker Operator role in administration of Docker Enterprise Edition Advanced.

Docker Troubleshooting & Support : (2 Days, $1995) - https://success.docker.com/training/courses/docker-support-troubleshooting

The Docker Troubleshooting & Support course is a role-based course designed for an organization’s support teams to troubleshoot the variety of issues that arise in their Docker journey.

Docker Security: (1 Day, $995) - https://success.docker.com/training/courses/docker-security

This is the Docker Security course for your entire organization. Get everyone “on-the-same page” and working together to secure your Dockerized environment. This hands-on workshop style course will give your team an overview of important security features and best practices to protect your containerized services.

   **Target audience**:  Startups/SMEs 

   **Difficulty**: Medium
#########################