Support
=======

Contest Participants Support
-----------------

If you have any further questions or issues while using UNICORN, please contact with contest@unicorn-project.eu.



Bugs & Support Issues
~~~~~~~~~~~~~~~~~~~~~

You can file bug reports on our `GitLab issue tracker`_,
and they will be addressed as soon as possible.
**Support is a volunteer effort**,
and there is no guaranteed response time.


Reporting Issues
~~~~~~~~~~~~~~~~

When reporting a bug,
please include as much information as possible that will help us solve this issue.
This includes:

* Project/Company name
* Action taken
* Expected result
* Actual result


.. _GitLab Issue Tracker: https://gitlab.com/unicorn-project/unicorn-reporting/issues
